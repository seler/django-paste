from functools import update_wrapper

from django.contrib import admin
from django.contrib.admin.utils import unquote
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.html import escape
from django.utils.text import capfirst
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from paste.models import Paste


def get_content_type_for_model(obj):
    # Since this module gets imported in the application's root package,
    # it cannot import models from other applications at the module level.
    from django.contrib.contenttypes.models import ContentType
    return ContentType.objects.get_for_model(obj, for_concrete_model=False)


class PasteAdmin(admin.ModelAdmin):
    list_display = (
        'get_view_link',
        'user',
        'lexer',
        'created',
    )
    list_display_links = None
    list_filter = ()
    list_select_related = False
    list_per_page = 100
    list_max_show_all = 200
    list_editable = ()
    search_fields = ()
    date_hierarchy = None
    save_as = False
    save_on_top = False
    preserve_filters = True
    inlines = []

    # Custom templates (designed to be over-ridden in subclasses)
    add_form_template = None
    change_form_template = None
    change_list_template = None
    delete_confirmation_template = None
    delete_selected_confirmation_template = None
    object_history_template = None

    # Actions
    actions = []
    actions_on_top = True
    actions_on_bottom = False
    actions_selection_counter = True

    def get_view_link(self, obj):
        return """<a href="{}">{}</a>""".format(obj.get_absolute_url(), obj)
    get_view_link.allow_tags = True
    get_view_link.short_description = Paste._meta.verbose_name

    def get_urls(self):
        from django.conf.urls import url

        urlpatterns = super(PasteAdmin, self).get_urls()

        # remove change view
        urlpatterns = filter(lambda u: u.name != 'paste_paste_change', urlpatterns)

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        paste_urlpatterns = [
            url(r'^(.+)/change/$', wrap(self.change_view), name='paste_paste_change'),
            url(r'^(.+)/raw/$', wrap(self.raw_view), name='paste_paste_raw'),
            url(r'^(.+)/$', wrap(self.view_view), name='paste_paste_view'),
        ]

        urlpatterns.extend(paste_urlpatterns)
        return urlpatterns

    def get_model_perms(self, request):
        perms = super(PasteAdmin, self).get_model_perms(request)
        perms.update({
            'view': self.has_view_permission(request),
        })
        return perms

    def has_view_permission(self, request, obj=None):
        return True
        return request.user.has_perm("paste.view")

    def view_view(self, request, object_id, extra_context=None):
        model = self.model
        obj = self.get_object(request, unquote(object_id))
        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_text(model._meta.verbose_name),
                'key': escape(object_id),
            })

        if not self.has_view_permission(request, obj):
            raise PermissionDenied

        # Then get the history for this object.
        opts = model._meta

        context = dict(
            self.admin_site.each_context(request),
            title=_('View Paste: %s') % force_text(obj),
            module_name=capfirst(force_text(opts.verbose_name_plural)),
            object=obj,
            opts=opts,
            preserved_filters=self.get_preserved_filters(request),
        )
        context.update(extra_context or {})

        request.current_app = self.admin_site.name

        return TemplateResponse(request, [
            "admin/paste/paste/view.html",
            "admin/paste/view.html",
            "admin/view.html"
        ], context)

    def raw_view(self, request, object_id, extra_context=None):
        "The 'history' admin view for this model."
        model = self.model
        obj = self.get_object(request, unquote(object_id))
        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_text(model._meta.verbose_name),
                'key': escape(object_id),
            })

        if not self.has_view_permission(request, obj):
            raise PermissionDenied
        a

        return HttpResponse(obj.content, content_type="text/plain")

    @csrf_exempt
    def add_view(self, *args, **kwargs):
        return super(PasteAdmin, self).add_view(*args, **kwargs)

admin.site.register(Paste, PasteAdmin)
