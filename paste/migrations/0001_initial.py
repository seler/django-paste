# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Paste',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hash_id', models.CharField(max_length=16, verbose_name='Secret ID', blank=True)),
                ('title', models.CharField(max_length=120, verbose_name='Title', blank=True)),
                ('content', models.TextField(verbose_name='Content')),
                ('content_colorized', models.TextField(verbose_name='Highlighted Content', blank=True)),
                ('lexer', models.CharField(default=b'python', max_length=30, verbose_name='Lexer')),
                ('created', models.DateTimeField(verbose_name='created', blank=True)),
                ('user', models.ForeignKey(verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-created',),
                'get_latest_by': 'created',
                'verbose_name': 'paste',
                'verbose_name_plural': 'pastes',
            },
        ),
    ]
