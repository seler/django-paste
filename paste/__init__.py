__version__ = '0.1'

default_app_config = 'paste.apps.PasteConfig'

from .debug import intercept_debug
intercept_debug()
