
paste_template = """
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script>
    $(document).ready(function(){
        $("#pasteform").submit(function(event){
            alert("As you can see, the link no longer took you to jquery.com");
            event.preventDefault();
        });
    });
</script>
"""

import django.views.debug
from paste.highlight import LEXER_TRACEBACK


def intercept_debug():
    #tag = '</body>'
    #django.views.debug.TECHNICAL_500_TEMPLATE = \
    #    django.views.debug.TECHNICAL_500_TEMPLATE.replace(tag, paste_template + tag)

    tag = '<form action="http://dpaste.com/" name="pasteform" id="pasteform" method="post">'
    new_tag = """<form action="/admin/paste/paste/add/" name="pasteform" id="pasteform" method="post">
    <input type="hidden" name="lexer" value="{}">""".format(LEXER_TRACEBACK)
    django.views.debug.TECHNICAL_500_TEMPLATE = \
        django.views.debug.TECHNICAL_500_TEMPLATE.replace(tag, new_tag)
