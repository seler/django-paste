import datetime
import string

from django.conf import settings
from django.db import models
from django.db.models import permalink
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from paste.highlight import LEXER_DEFAULT, pygmentize


class Paste(models.Model):
    hash_id = models.CharField(_(u'Secret ID'), max_length=16, blank=True)
    title = models.CharField(_(u'Title'), max_length=120, blank=True)
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name=_(u'User'), blank=True, null=True)
    content = models.TextField(_(u'Content'))
    content_colorized = models.TextField(_(u'Highlighted Content'), blank=True)
    lexer = models.CharField(_(u'Lexer'), max_length=30, default=LEXER_DEFAULT)
    created = models.DateTimeField(verbose_name=_(u"created"), blank=True)

    class Meta:
        verbose_name = _(u"paste")
        verbose_name_plural = _(u"pastes")
        ordering = ('-created', )
        get_latest_by = "created"

    def __unicode__(self):
        return self.hash_id

    def get_linecount(self):
        return len(self.content.splitlines())

    def content_splitted(self):
        return self.content_colorized.splitlines()

    def save(self):
        if not self.pk:
            self.created = datetime.datetime.now()
        self.content_colorized = pygmentize(self.content, self.lexer)
        super(Paste, self).save()

    @permalink
    def get_absolute_url(self):
        return ('admin:paste_paste_view', (self.pk,))


t = string.letters + string.digits


def generate_hash_id(pk):
    """
    Converts `pk` into number in base `len(t)`.
    """
    base = len(t)
    if pk == 0:
        return t[0]

    digits = []
    while pk:
        digits.append(t[pk % base])
        pk /= base
    digits.reverse()
    return ''.join(digits)


def assign_hash_id(sender, instance, **kwargs):
    if not instance.hash_id:
        instance.hash_id = generate_hash_id(instance.pk)
        instance.save()


post_save.connect(assign_hash_id, Paste)
