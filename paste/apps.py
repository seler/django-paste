from django.apps import AppConfig


class PasteConfig(AppConfig):
    name = 'paste'
    label = 'paste'
    verbose_name = "Paste"
